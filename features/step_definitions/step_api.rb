Dado("que eu faça um get no endpoint para retornar o title dos results") do
    url = "https://swapi.co/api/films/"
    metodo = "GET"
    @api.realiza_requisicao(metodo,url)
end

E("valido o status code da resposta do servico") do
    body = $body
    @api.valida_status_code(body)
end

Entao("exibo o titulo de cada filme retornado") do
    @api.printa_resultados
end
