Dado("que tenha acessado o site") do
  #Acessa a pagina
  visit(DATA['url_site'])
  #Valida o carregamento
  assert_selector(DATA['btn_login'])
end

Dado("preencha o campo email e password com um usuario valido") do
  @login.preenche_campos
end

Dado("clico no botão LOGIN") do
  @login.realiza_login
end

Dado("valido o login com sucesso") do
  @login.valida_login
end

Dado("que eu esteja logado") do
  #Acessa a pagina
  visit(DATA['url_site'])
  #Valida o carregamento da pagina
  assert_selector(DATA['btn_login'])
  #Preenche campos
  @login.preenche_campos
  #Clica botao LOGIN
  @login.realiza_login
  #Valida login com sucesso
  @login.valida_login
end
